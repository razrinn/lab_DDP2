# Repository Tutorial & Lab
Dasar-dasar Pemrograman 2 - CSGE601021 | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Genap 2017/2018
***


## Daftar Isi

Repository ini akan berisi materi-materi Tutorial & Lab DDP 2.

1. Lab
    1. [Lab 1](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_1/README.md) - Pengenalan Java & Git
    2. [Lab 2](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_2/README.md) - Konsep Dasar Pemrograman Java
    3. [Lab 3](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_3/README.md) - Rekursif
<<<<<<< HEAD
=======
    4. [Lab 4](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_4/README.md) - Object Oriented Programming
<<<<<<< HEAD
>>>>>>> 780da5113ed284e101c6d245285c6d13dfa1eddb
=======
    5. [Lab 5](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_5/README.md) - Array dan ArrayList
<<<<<<< HEAD
>>>>>>> edbd82450cfe1d4db22e1e1562fa74ae43830183

=======
	6. [Lab 6](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_6/README.md) - Studi Kasus OOP
<<<<<<< HEAD
<<<<<<< HEAD
>>>>>>> 0d65ee7b6b68537950414ed5d4416d21e131fcce
***
=======
=======
	7. [Lab 7](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_7/README.md) - Inheritance
<<<<<<< HEAD
>>>>>>> 7760e247397b48e996f794dbcf7506ab4f040e83
=======
	8. [Lab 8](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_8/README.md) - Polymorphism
<<<<<<< HEAD
>>>>>>> b9f89f605df5bd780c8457143a47f523af53251b
=======
	9. [Lab 9](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_9/README.md) - Packaging dan API
<<<<<<< HEAD
>>>>>>> 41d825197b6b2c762feddfa80b1fd1ffa05197f6
=======
    10. [Lab 10](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_10/README.md) - Exception dan GUI
>>>>>>> 3beb305fc26d51f672300d81bae0c4c7b2d69321

>>>>>>> f78c0e7742338c8c37b272c05a7ad43888206b02

_Tools_ yang akan digunakan dalam mata kuliah ini antara lain :

- Java Development Kit (JDK) 8
- Git
- Notepad++ (atau text editor sejenisnya)
- Integrated Development Environment (IDE)
- Gradle
- GitLab Account

Pastikan kalian telah menginstall / memiliki _tools_ diatas, jika belum bisa melihat petunjuk penginstallan dan
konfigurasinya [disini](https://drive.google.com/file/d/1c1AA-9ju1S82-NYyV7EMyPNwScPpMQsr/view?usp=sharing)

Kontak Informasi :

- Line Dek Depe : [@nhz2170m](https://line.me/R/ti/p/%40nhz2170m)
