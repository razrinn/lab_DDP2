package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.util.XoxoMessage;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    private int encCounter = 1;
    private int decCounter = 1;
    private String encPath = System.getProperty("user.dir") + "\\encrypted\\";
    private String decPath = System.getProperty("user.dir") + "\\decrypted\\";
    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(ae -> encrypt());
        gui.setDecryptFunction(ae -> decrypt());
    }

    private void encrypt(){
        String message = gui.getMessageText();
        if (message.length() == 0) {
            JOptionPane.showMessageDialog(gui.getMainFrame(), "Please fill the message field", "Error", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        String kissKey = gui.getKeyText();
        if (kissKey.length() == 0) {
            JOptionPane.showMessageDialog(gui.getMainFrame(), "Please fill the key field", "Error", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        String seed = gui.getSeedText();
        if (seed.length() == 0) {
            JOptionPane.showMessageDialog(gui.getMainFrame(), "Please fill the seed field", "Error", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        XoxoEncryption xoxoEnc = null;
        XoxoMessage encryptMsg = null;
        try{
            xoxoEnc = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException | InvalidCharacterException error) {
            gui.appendLog(error.getMessage());
        }

        try {
            if (seed.equals("") || seed.equals("DEFAULT_SEED")){
                encryptMsg = xoxoEnc.encrypt(message);
            }
            else {
                encryptMsg = xoxoEnc.encrypt(message, Integer.parseInt(seed));
            }
        } catch (RangeExceededException | SizeTooBigException | InvalidCharacterException error) {
            JOptionPane.showMessageDialog(gui.getMainFrame(), error.getMessage(), "Error detected", JOptionPane.INFORMATION_MESSAGE);
            gui.appendLog("ENCRYPTION ERROR! " + error.getMessage());
        }
        generateEncFile(encryptMsg);
    }

    private void decrypt(){
        String message = gui.getMessageText();
        if (message.length() == 0) {
            JOptionPane.showMessageDialog(gui.getMainFrame(), "Please fill the message field", "Error", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        String hugKey = gui.getKeyText();
        if (hugKey.length() == 0) {
            JOptionPane.showMessageDialog(gui.getMainFrame(), "Please fill the key field", "Error", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        XoxoDecryption xoxoDec = new XoxoDecryption(hugKey);
        try{
            xoxoDec = new XoxoDecryption(hugKey);
        } catch (KeyTooLongException | InvalidCharacterException error) {
            gui.appendLog(error.getMessage());
        }
        int seed;
        if (gui.getSeedText().length() == 0) {
            JOptionPane.showMessageDialog(gui.getMainFrame(), "Please fill the seed field", "Error", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        String decryptMsg = null;
        try {
            if (gui.getSeedText().equals("") || gui.getSeedText().equals("DEFAULT_SEED")) seed = HugKey.DEFAULT_SEED;
            else seed = Integer.parseInt(gui.getSeedText());

            decryptMsg = xoxoDec.decrypt(message, seed);
        } catch (RangeExceededException | SizeTooBigException | InvalidCharacterException error) {
            JOptionPane.showMessageDialog(gui.getMainFrame(), error.getMessage(), "Error detected", JOptionPane.INFORMATION_MESSAGE);
            gui.appendLog("DECRYPTION ERROR! " + error.getMessage());
        }
        generateDecFile(decryptMsg);
    }

    public void generateEncFile(XoxoMessage message) {
        File output = new File(encPath + "encrypted_" + encCounter
                + ".enc");
        try {
            output.createNewFile();
            FileWriter writer = new FileWriter(output);
            writer.write(message.getEncryptedMessage());
            writer.flush();
            writer.close();
            gui.appendLog("Message \"" + gui.getMessageText() + "\" is Encrypted to \"" +message.getEncryptedMessage()
                    + "\"");
            encCounter++;
        } catch (IOException e) {
            gui.appendLog("Encryption unsuccessful :(");
        }
    }
    public void generateDecFile (String message) {
        File output = new File(decPath + "\\decrypted_" + decCounter
                + ".txt");
        try {
            output.createNewFile();
            FileWriter writer = new FileWriter(output);
            writer.write(message);
            writer.flush();
            writer.close();
            gui.appendLog("Message \"" + gui.getMessageText() + "\" is Decrypted to \"" + message + "\"");
            decCounter++;
        } catch (IOException e) {
            gui.appendLog(("Decryption unsuccessful :("));
        }
    }
}