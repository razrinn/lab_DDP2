package xoxo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    private JPanel panel;
    private JFrame mainFrame;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        mainFrame = new JFrame("XoXo Message Encryptor and Decryptor");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setPreferredSize(new Dimension(700, 500));
        mainFrame.add(initPanel());
        initButtons();
        mainFrame.setVisible(true);
        mainFrame.setResizable(false);
        mainFrame.pack();
    }

    private JPanel initPanel(){
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        return panel;
    }

    private void initButtons(){
        JPanel message = new JPanel();
        JPanel key = new JPanel();
        JPanel seed = new JPanel();
        JPanel button = new JPanel();
        logField = new JTextArea("XoXo Encryptor/Decryptor Log\n======================\n");
        logField.setSize(500,300);
        logField.setFont(logField.getFont().deriveFont(15f));
        logField.setEditable(false);

        JLabel labelMsg = new JLabel("Message : ");
        JLabel labelKey = new JLabel("Key     :  ");
        JLabel labelSeed = new JLabel("Seed   :  ");

        messageField = new JTextField();
        messageField.setPreferredSize(new Dimension(400, 30));
        keyField = new JTextField();
        keyField.setPreferredSize(new Dimension(100, 30));
        seedField = new JTextField();
        seedField.setPreferredSize(new Dimension(100, 30));

        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton(("Decrypt"));
        encryptButton.setPreferredSize(new Dimension(100,30));
        decryptButton.setPreferredSize(new Dimension(100,30));

        message.add(labelMsg);
        message.add(messageField);
        key.add(labelKey);
        key.add(keyField);
        seed.add(labelSeed);
        seed.add(seedField);
        button.add(encryptButton);
        button.add(decryptButton);

        message.setAlignmentX(Component.CENTER_ALIGNMENT);
        key.setAlignmentX(Component.CENTER_ALIGNMENT);
        seed.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);

        panel.add(message);
        panel.add(key);
        panel.add(seed);
        panel.add(button);
        panel.add(logField);
        panel.add(logField);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    public String getSeedText(){
        return seedField.getText();
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }

    public JButton getDecryptButton() {
        return decryptButton;
    }

    public JButton getEncryptButton() {
        return encryptButton;
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}