package xoxo.exceptions;

/**
 * <write the documentation>
 */
public class InvalidCharacterException extends RuntimeException {
    public InvalidCharacterException(String message){
        super(message);
    }
}