import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Ray Azrin Karim, NPM 1706044111, Kelas E, GitLab Account: rayazrin19
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);

		String warning = "WARNING: Keluarga ini tidak perlu direlokasi!";
		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		short panjang = Short.parseShort(input.nextLine());
		if(panjang < 0 || panjang > 250){
			System.out.println(warning);
			return;
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		short lebar = Short.parseShort(input.nextLine());
		if(lebar < 0 || lebar > 250){
			System.out.println(warning);
			return;
		}
		System.out.print("Tinggi Tubuh (cm)      : ");
		short tinggi = Short.parseShort(input.nextLine());
		if(tinggi < 0 || tinggi > 250){
			System.out.println(warning);
			return;
		}
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.nextLine());
		if(berat < 0 || berat > 150){
			System.out.println(warning);
			return;
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		byte makanan = Byte.parseByte(input.nextLine());
		if(makanan > 20){
			System.out.println(warning);
			return;
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		byte jumlahCetakan = Byte.parseByte(input.nextLine());


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		short rasio = (short) (berat*1000000/(panjang*lebar*tinggi));

		for (int i = 1; i<= jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (catatan.length()==0) catatan = "Tidak ada catatan tambahan";
			else catatan = "Catatan: " + catatan;

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "\nDATA SIAP DICETAK UNTUK" + penerima.toUpperCase() + "\n-----------------\n" +
			nama + " - " + alamat + "\nLahir pada tanggal" + tanggalLahir + "\nRasio Berat Per Volume " +
			rasio + " kg/m^3 \n" + catatan + "\n\n";
			System.out.print(hasil);
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		short total = 0;
		for (int i=0; i< nama.length(); i++){
			char karakter = nama.charAt(i);
			int ascii = (int) karakter;
			total += (short) ascii;
		}

		int kodeKeluarga = ((panjang * lebar * tinggi) + total) % 10000;

		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.substring(0,1) + String.valueOf(kodeKeluarga);

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (int) (50000 * 365 * makanan);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		short tahunLahir = Short.parseShort(tanggalLahir.substring(6)) ; // lihat hint jika bingung
		short umur = (short) (2018 - tahunLahir);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String tempat = "";
		String kabupaten = "";
		
		if(19 <= umur && umur <= 1018){
			if(0 <= anggaran && anggaran <= 100000000){
				tempat = "Teksas";
				kabupaten = "Sastra";
			}
			else if(anggaran > 100000000){
				tempat = "Mares";
				kabupaten = "Margonda";
			}
		}
		else{
			tempat = "PPMT";
			kabupaten = "Rotunda";
		}


		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "REKOMENDASI APARTEMEN \n-------------------- \n MENGETAHUI\t : Identitas keluarga: "
		+ nama + " - " + nomorKeluarga + "\nMENIMBANG\t : Anggaran makanan tahunan: Rp" + anggaran + "\n\t\t   Umur kepala keluarga: "
		+ umur + " tahun \nMEMUTUSKAN\t : keluarga " + nama + " akan ditempatkan di: " + tempat + ", kabupaten " + kabupaten;
		System.out.println(rekomendasi);

		input.close();
	}
}
