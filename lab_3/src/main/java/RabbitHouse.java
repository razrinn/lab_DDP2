import java.util.Scanner;

public class RabbitHouse{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan nama kelinci : ");
        String mode = input.next();
        String namaInduk = input.next();
        int panjangNama = namaInduk.length();
        System.out.print("Jumlah anak kelinci : ");
        if(mode.equals("normal")){
            System.out.print(cekNormal(panjangNama)+1);
        }
        else if(mode.equals("palindrom")){
            System.out.print(hitungPalindrome(namaInduk));
        }
    }
    
    public static int cekNormal(int nama){
        if (nama <= 1){
            return 0;
        }
        else{
            return nama * (1 + cekNormal(nama-1));
        }
    }
    
    public static boolean cekPalindrome(String str) {
        return str.equals(new StringBuilder(str).reverse().toString());
    }
    
    public static int hitungPalindrome(String str){
        int x;
        if(cekPalindrome(str) == true){
            return 0;
        }
        else{
            x = 1;
            for(int i = 0; i < str.length();i++){
                x += hitungPalindrome(str.substring(0,i) + str.substring(i+1));
            }
            return x;
        }
    }
}