public class Manusia{
    //Inisiasi
    private String nama;   
    private int umur;
    private int uang = 50000;
    private float kebahagiaan = 50;

    //Constructor
    public Manusia(String nama, int umur){
        this.nama = nama;
        this.umur = umur;
    }

    public Manusia(String nama, int umur, int uang){
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
    }

    //Setter dan Getter
    public void setNama(String nama){
		this.nama = nama;
    }
    
	public String getNama(){
		return nama;
    }
    
	public void setUmur(int umur){
		this.umur = umur;
	}
    
    public int getUmur(){
		return umur;
    }
    
	public void setUang(int uang){
		this.uang = uang;
	}
	public int getUang(){
		return uang;
    }
    
	public void setKebahagiaan(float kebahagiaan){
		if (kebahagiaan > 100){
			this.kebahagiaan = 100;
		}
		else if (kebahagiaan < 0 ){
			this.kebahagiaan = 0;
		}
		else{
			this.kebahagiaan = kebahagiaan;
		}
	}
	
	public float getKebahagiaan(){
		return kebahagiaan;
	}
    
    //Behaviour
	public void beriUang(Manusia penerima){
		short total = 0;
		String namaPenerima = penerima.getNama();
		for (int i=0; i< namaPenerima.length(); i++){
			char karakter = namaPenerima.charAt(i);
			int ascii = (int) karakter;
			total += (short) ascii;
		}
		int uangAkhir = total*100;
		if(this.uang >= uangAkhir){
			penerima.setUang(penerima.getUang() + uangAkhir);
			setUang(this.uang - uangAkhir);
			penerima.setKebahagiaan(penerima.getKebahagiaan() + ((float) uangAkhir/6000));
			setKebahagiaan(this.kebahagiaan + ((float) uangAkhir/6000));
			System.out.println(this.nama + " memberi uang sebanyak " + uangAkhir + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
		}
		else{
			System.out.println(this.nama + " ingin memberi uang kepada" + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
		}
		
	}
	
	public void beriUang(Manusia penerima, int jumlah){
		if(this.uang >= jumlah){
			penerima.setUang(penerima.getUang() + jumlah);
			this.uang -= jumlah;
			penerima.setKebahagiaan(penerima.getKebahagiaan() + ( (float) jumlah/6000));
			setKebahagiaan(this.kebahagiaan + ((float) jumlah/6000));
			System.out.println(this.nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
		}
		else{
			System.out.println(this.nama + " ingin memberi uang kepada" + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
		}
	}

	public void bekerja(int durasi, int bebanKerja){
		int BebanKerjaTotal = (int) ((float) durasi * bebanKerja);
		if(this.umur < 18){
			System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur :D");
		}
		else{
			if(BebanKerjaTotal <= this.kebahagiaan){
				setKebahagiaan(this.kebahagiaan - BebanKerjaTotal);
				setUang(this.uang + (BebanKerjaTotal * 10000));
				System.out.println(this.nama + " bekerja full time, total pendapatan : " + (BebanKerjaTotal * 10000));
			}
			else{
				int durasiBaru = (int) ((float) this.kebahagiaan / bebanKerja);
				BebanKerjaTotal = durasiBaru * bebanKerja;
				setKebahagiaan(this.kebahagiaan - BebanKerjaTotal);
				setUang(this.uang + (BebanKerjaTotal * 10000));
				System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + (BebanKerjaTotal * 10000));
				System.out.println("Jumlah uang ditambahkan dengan Pendapatan");
			}
		}
	}

	public void rekreasi(String namaTempat){
		int biaya = namaTempat.length() * 10000;
		if(this.uang > biaya){
			setKebahagiaan(this.kebahagiaan + namaTempat.length());
			setUang(this.uang - biaya);
			System.out.println(this.nama + " berekreasi di " + namaTempat + " , " + this.nama + " senang :)");
		}
		else{
			System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
		}
	}

	public void sakit(String namaPenyakit){
		setKebahagiaan(this.kebahagiaan - namaPenyakit.length());
		System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :O");
	}

	public String toString(){
		return "Nama\t\t: " + getNama() + "\nUmur\t\t: " + getUmur() + "\nUang\t\t: " + getUang() + "\nKebahagiaan\t: " + getKebahagiaan();
	}
}