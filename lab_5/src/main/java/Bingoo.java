import java.util.Scanner;

public class Bingoo{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Number [][] tempArr = new Number[5][5];
        Number [] tempStatesArr = new Number[100];

        System.out.println("Masukkan 25 angka, dimana setiap barisnya terdiri dari 5 angka yang dipisahkan spasi");
        for(int m = 0; m < 5;m++){
            for(int n = 0; n < 5;n++){
                int angka = input.nextInt();
                if (tempStatesArr[angka] == null) {
                    tempStatesArr[angka] = new Number(angka, m, n);
                }
                tempArr[m][n] = tempStatesArr[angka];
            }
            input.nextLine();
        }
        BingoCard pemain = new BingoCard(tempArr, tempStatesArr);

        System.out.println("Masukkan perintah yang ingin dilakukan (MARK [angka], INFO, dan RESTART)");
        while(pemain.isBingo() == false){
            String mode = input.next();
            switch (mode){
                case "MARK":
                    int angka = input.nextInt();
                    System.out.println(pemain.markNum(angka));
					break;
                case "INFO":
                    System.out.println(pemain.info());
                    break;
                case "RESTART":
                    pemain.restart();
                    break;
                default:
                    System.out.println("Incorrect command");
            }
            if(pemain.isBingo()){
				System.out.println("BINGO!");
				System.out.println(pemain.info());
				break;
			}
        }
        input.close();
        
    }
}