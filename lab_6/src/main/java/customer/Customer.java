package customer;
import ticket.Ticket;
import theater.Theater;
import movie.Movie;

public class Customer{
	String name;
	String gender;
	int age;
	String typeAge;

	public Customer(String name,  String gender, int age){
		this.name = name;
		this.gender = gender;
		this.age = age;

	}

	public Ticket orderTicket(Theater theater, String title, String day, String String3D){
		int ticketCounter = 0;
		for(Ticket ticket : theater.getTicket()){
			if(ticket.getFilm().getTitle().equals(title) && ticket.getDay().equals(day) && ticket.get3D().equals(String3D)){
				if(ticket.getFilm().getRating().equals("Umum")){
					theater.setBalance(ticket.getPrice());
					System.out.println(this.name + " telah membeli tiket " + title + " jenis " + String3D + " di " + theater.getName() + " pada hari " + ticket.getDay() + " seharga Rp. " + ticket.getPrice());
					return ticket;
				}
				else if(ticket.getFilm().getRating().equals("Remaja")){
					if (this.age >= 13) {
						theater.setBalance(ticket.getPrice());
						System.out.println(this.name + " telah membeli tiket " + title + " jenis " + String3D + " di " + theater.getName() + " pada hari " + ticket.getDay() +  " seharga Rp. " + ticket.getPrice());
						return ticket;
					}
					else{
						System.out.println(this.name + " masih belum cukup umur untuk menonton " + title + " dengan rating " + String3D);
						return null;
					}
				}
				else if(ticket.getFilm().getRating().equals("Dewasa")){
					if (this.age >= 17) {
						theater.setBalance(ticket.getPrice());
						System.out.println(this.name + " telah membeli tiket " + title + " jenis " + String3D + " di " + theater.getName() + " pada hari " + ticket.getDay() +  " seharga Rp. " + ticket.getPrice());
						return ticket;
					}
					else{
						System.out.println(this.name + " masih belum cukup umur untuk menonton " + title + " dengan rating " + ticket.getFilm().getRating());
						return null;
					}
				}
			}
			ticketCounter++;
			if(ticketCounter == theater.getTicket().size()){
				System.out.println("Tiket untuk film " + title + " jenis " + String3D + " dengan jadwal " + day + " tidak tersedia di " + theater.getName());
			}
		}
		return null;
	}

	public void findMovie(Theater theater, String title){
		for(int i = 0; i < theater.getMovie().length; i++){
			if(theater.getMovie()[i].getTitle().equals(title)){
				System.out.println(theater.getMovie()[i].printMovie());
			}
			else{
				if(i == theater.getMovie().length - 1){
					System.out.println("Film " + title + " yang dicari " + this.name + " tidak ada di bioskop " + theater.getName());
				}
			}
		}
	}

}