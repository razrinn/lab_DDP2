package movie;

public class Movie{
	private String title;
	private String genre;
	private int duration;
	private String rating;
	private String type;

	public Movie(String title, String rating, int duration, String genre, String type){
		this.title = title;
		this.rating = rating;
		this.duration = duration;
		this.genre = genre;
		this.type = type;
	}
	

	public String getTitle(){
		return  this.title;
	}

	public String getGenre(){
		return this.genre;
	}

	public int getDuration(){
		return this.duration;
	}

	public String getRating(){
		return this.rating;
	}

	public String printMovie(){
		return "------------------------------------------------------------------\n" +
			"Judul\t: " + this.title + 
			"\nGenre\t: " + this.genre + 
			"\nDurasi\t: " + this.duration + " menit" +
			"\nRating\t: " + this.rating + 
			"\nJenis\t: Film " + this.type + 
			"\n------------------------------------------------------------------";
	}
}