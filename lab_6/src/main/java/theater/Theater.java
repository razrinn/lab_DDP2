package theater;
import movie.Movie;
import ticket.Ticket;
import java.util.ArrayList;

public class Theater{
	private String name;
	private int balance;
	private ArrayList<Ticket> ticket = new ArrayList<Ticket>();
	private Movie[] movie;

	public Theater(String name, int balance, ArrayList<Ticket> ticket, Movie[] movie){
		this.name = name;
		this.balance = balance;
		this.ticket = ticket;
		this.movie = movie;
	} 

	public String getName(){
		return this.name;
	}

	public int getBalance(){
		return this.balance;
	}

	public ArrayList<Ticket> getTicket(){
		return this.ticket;
	}


	public Movie[] getMovie(){
		return this.movie;
	}

	public void setBalance(int balance){
		this.balance += balance;
	}

	public void printInfo(){
		String movieList = "";
		for(int i = 0; i < this.movie.length; i++){
			if(i < this.movie.length - 1){
				movieList += (movie[i].getTitle() + ", ");
			}
			else{
				movieList += movie[i].getTitle();
			}
		}
		System.out.println("------------------------------------------------------------------\n" +
			"Bioskop\t\t\t: " + this.name + 
			"\nSaldo kas\t\t: " + this.balance + 
			"\nJumlah tiket tersedia\t: " + this.ticket.size() + 
			"\nDaftar Film tersedia\t: " + movieList + 
			"\n------------------------------------------------------------------");
	}


	public static void printTotalRevenueEarned(Theater[] theaters){
		int totalBalance = 0;
		for(int i = 0; i < theaters.length; i++){
			totalBalance += theaters[i].getBalance();
		}
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalBalance + 
		"\n------------------------------------------------------------------");
			for(int i = 0; i < theaters.length; i++){
				if(i < theaters.length - 1){
					System.out.println("Bisokop\t\t: " + theaters[i].getName() + 
					"\nSaldo Kas\t: Rp. " + theaters[i].getBalance() + "\n");
				}
				else{
					System.out.println("Bisokop\t\t: " + theaters[i].getName() + 
					"\nSaldo Kas\t: Rp. " + theaters[i].getBalance() + 
					"\n------------------------------------------------------------------");
				}
			}
	}
}