package ticket;
import movie.Movie;

public class Ticket{
	private Movie film;
	private String day;
	private boolean type3D;
	private int price;

	public Ticket(Movie film, String day, Boolean type3D){
		this.price = 60000;
		if(day.equals("Sabtu") || day.equals("Minggu")){
			price = 100000;
		}
		if(type3D){
			price += price/5;
		}
		
		this.film = film;
		this.day = day;
		this.type3D = type3D;

	}

	public int getPrice(){
		return this.price;
	}

	public Movie getFilm(){
		return this.film;
	}

	public String getDay(){
		return this.day;
	}

	public String get3D(){
		if(this.type3D == true){
			return "3 Dimensi";
		}
		else{
			return "Biasa";
		}
	}

	public void printTicket(){
		System.out.println("------------------------------------------------------------------\n" +
			"Film\t\t: " + film.getTitle() + 
			"\nJadwal tayang\t: " + this.day + 
			"\nJenis\t\t: " + get3D() + 
			"\n------------------------------------------------------------------");
	}

}