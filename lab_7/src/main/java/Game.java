import character.*;

import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<Player>();
    private ArrayList<Player> EatenAll = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     *
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (Player chara : player) {
            if (chara.getName().equals(name)) {
                return chara;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (find(chara) == null) {
            switch (tipe) {
                case "Human":
                    Player hum = new Human(chara, hp);
                    player.add(hum);
                    break;
                case "Magician":
                    Player mag = new Magician(chara, hp);
                    player.add(mag);
                    break;
                case "Monster":
                    Player mon = new Monster(chara, hp);
                    player.add(mon);
                    break;
                default:
                    return "Tipe yang dimasukkan tidak tersedia";
            }
            return chara + " ditambahkan ke dalam game";
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        if (tipe.equals("Monster")) {
            if (find(chara) == null) {
                Monster mon = new Monster(chara, hp, roar);
                player.add(mon);
                return chara + " sudah ditambahkan ke dalam game";
            } else {
                return "Sudah ada karakter bernama " + chara;
            }
        } else {
            return "Tipe yang dimasukkan tidak valid";
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     *
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        if (find(chara) != null) {
            player.remove(find(chara));
            return chara + " dihapus dari game";
        } else {
            return "Tidak ada " + chara;
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     *
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        Player p = find(chara);
        String status = "";
        if (p != null) {
            if (p.getIsAlive()) {
                status = "Masih hidup";
            } else {
                status = "Sudah meninggal dunia dengan damai";
            }
            return p.getType() + " " + p.getName() +
                    "\n" + "HP: " + p.getHp() + "\n" +
                    status + "\n" +
                    ((p.getEatenPlayer().size() != 0) ? "Memakan " + diet(p.getName()) : "Belum memakan siapa siapa");
        } else {
            return "Tidak ada" + chara;
        }
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     *
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status() {

        if (player.size() > 0) {
            String output = "";
            for (Player pl : player) {
                output += status(pl.getName()) + "\n";
            }
            return output;
        } else {
            return "Tidak ada pemain";
        }
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     *
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        Player pl = find(chara);
        String output = "";
        ArrayList<Player> plList = pl.getEatenPlayer();
        if (pl != null) {
            for (int i = 0; i < plList.size(); i++) {
                if (i < plList.size() - 1) {
                    output += plList.get(i).getType() + " " + plList.get(i).getName() + ", ";
                } else {
                    output += plList.get(i).getType() + " " + plList.get(i).getName();
                }
            }
            return output;
        } else {
            return "Tidak ada " + chara;
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     *
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String output = "Termakan : ";
        if (EatenAll.size() != 0) {
            for (int i = 0; i < EatenAll.size(); i++) {
                if (i < EatenAll.size() - 1) {
                    output += EatenAll.get(i).getType() + " " + EatenAll.get(i).getName() + ", ";
                } else {
                    output += EatenAll.get(i).getType() + " " + EatenAll.get(i).getName();
                }
            }
            return output;
        } else {
            return "Belum ada yang termakan";
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        Player attacker = find(meName);
        Player attacked = find(enemyName);
        if (attacker != null && attacked != null) {
            if (attacker.getIsAlive()) {
                return attacker.attack(attacked);
            } else {
                return "Penyerang sudah meninggal dunia";
            }
        } else {
            return "Attacker/Attacked tidak ada";
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player attacker = find(meName);
        Player attacked = find(enemyName);
        if (attacker != null && attacked != null) {
            if (attacker.getIsAlive()) {
                if (attacker instanceof Magician) {
                    return ((Magician) attacker).burn(attacked);
                } else {
                    return meName + " tidak bisa melakukan burn";
                }
            } else {
                return "Penyerang sudah meninggal dunia";
            }
        } else {
            return "Attacker/Attacked tidak ada";
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        Player attacker = find(meName);
        Player attacked = find(enemyName);
        if (attacker != null && attacked != null) {
            if (attacker.getIsAlive()) {
                if (attacker.canEat(attacked)) {
                    remove(enemyName);
                    attacker.setHp(15);
                    addEatenAll(attacked);
                    attacker.addEatenPlayer(attacked);
                    return meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + attacker.getHp();
                } else {
                    return meName + " tidak bisa memakan " + enemyName;
                }
            }
            return "Penyerang sudah meninggal dunia";
        }
        return "Attacker/Attacked tidak ada";
    }

    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     *
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        if (find(meName) != null) {
            if (find(meName) instanceof Monster) {
                return ((Monster) find(meName)).roar();
            } else {
                return meName + " tidak bisa berteriak";
            }
        }
        return "Tidak ada " + meName;
    }

    public void addEatenAll(Player name){
        EatenAll.add(name);
    }

    public String printTree(String meName) {
        Player me = find(meName);
        if (me != null) {
            return me.printTree();
        }
        return "Tidak ada " + meName;
    }
}