package character;

import character.Player;

public class Human extends Player {
    public Human(String name, int hp) {
        super(name, hp);
        this.type = "Human";
    }
}