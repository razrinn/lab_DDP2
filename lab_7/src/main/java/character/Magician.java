package character;

import character.Player;

public class Magician extends Human {
    public Magician(String name, int hp) {
        super(name, hp);
        this.type = "Magician";
    }

    public String burn(Player enemy) {
        String matang = "";
        if (enemy instanceof Magician) {
            enemy.setHp(-20);
        } else {
            enemy.setHp(-10);
        }
        if (enemy.getHp() == 0) {
            enemy.setAlive();
            enemy.setBurn();
            matang = "\n dan matang";
        }
        return "Nyawa " + enemy.getName() + " " + enemy.getHp() + matang;

    }
}