package character;

import character.Player;

public class Monster extends Player {
    private String roar = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster(String name, int hp) {
        super(name, hp * 2);
        this.type = "Monster";
    }

    public Monster(String name, int hp, String roar) {
        super(name, hp * 2);
        this.roar = roar;
        this.type = "Monster";
    }

    public String getRoar() {
        return roar;
    }

    public String roar() {
        return getRoar();
    }
}


