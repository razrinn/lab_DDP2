package character;

import java.util.ArrayList;

public class Player {
    protected String name;
    protected String type;
    protected int hp;
    protected boolean isAlive = true;
    protected boolean isBurn;
    protected ArrayList<Player> eatenPlayer = new ArrayList<Player>();

    public Player(String name, int hp) {
        this.name = name;
        if (hp > 0) {
            this.hp = hp;
        } else {
            isAlive = false;
        }
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public String getType() {
        return type;
    }

    public boolean getIsAlive() {
        return isAlive;
    }

    public boolean getIsBurn() {
        return isBurn;
    }

    public void setAlive() {
        isAlive = false;
    }

    public void setBurn() {
        isBurn = true;
    }

    public void setHp(int hp) {
        if (this.hp + hp < 0) {
            this.hp = 0;
        } else {
            this.hp += hp;
        }
    }

    public void addEatenPlayer(Player name) {
        eatenPlayer.add(name);
    }

    public ArrayList<Player> getEatenPlayer() {
        return eatenPlayer;
    }

    public boolean canEat(Player enemy) {
        if (this.type.equals("Human")) {
            if (enemy.getType().equals("Human") || enemy.getType().equals("Magician")) {
                return false;
            } else if (enemy.getType().equals("Monster")) {
                if (enemy.getIsAlive() == false && enemy.getIsBurn() == true) {
                    return true;
                } else {
                    return false;
                }
            }
        } else if (this.type.equals("Magician")) {
            if (enemy.getType().equals("Human") || enemy.getType().equals("Magician")) {
                return false;
            } else if (enemy.getType().equals("Monster")) {
                if (enemy.getIsAlive() == false && enemy.getIsBurn() == true) {
                    return true;
                } else {
                    return false;
                }
            }
        } else if (this.type.equals("Monster")) {
            if (enemy.getIsAlive() == false) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public String attack(Player enemy) {
        if (enemy instanceof Magician) {
            enemy.setHp(-20);
        } else {
            enemy.setHp(-10);
        }
        if (enemy.getHp() == 0) {
            enemy.setAlive();
        }
        return "Nyawa " + enemy.getName() + " " + enemy.getHp();
    }

    public String printTree() {
        return printTree(this, "");
    }

    public String printTree(Player player, String indent) {
        String result = indent + "> " + player.getName() + " " + player.getType() + "\n";

        for (Player eaten : player.getEatenPlayer()) {
            result += printTree(eaten, indent + "  ");
        }

        return result;
    }
}
