import java.util.Scanner;
import corporate.*;

class Lab8 {
    public static void main(String[] args) {
        Corporate corp = new Corporate();
        Scanner input = new Scanner(System.in);
        int gajiMax = Integer.parseInt(input.nextLine());
        corp.setSALARY_TRESHOLD(gajiMax);
        while(true){
            String[] command = input.nextLine().split(" ");
            switch (command[0]){
                case "TAMBAH_KARYAWAN" :
                    System.out.println(corp.add(command[1], command[2], Integer.parseInt(command[3])));
                    break;
                case "STATUS" :
                    System.out.println(corp.status(command[1]));
                    break;
                case "GAJIAN" :
                    System.out.println(corp.payday());
                    break;
                case "TAMBAH_BAWAHAN" :
                    System.out.println(corp.addSubordinate(command[1], command[2]));
                    break;
                default :
                    System.out.println("Perintah tidak valid.");
                    break;
            }
        }
    }
}