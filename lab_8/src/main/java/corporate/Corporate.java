package corporate;

import employee.*;

import java.util.ArrayList;

public class Corporate {
    private static ArrayList<Employee> arrEmployee = new ArrayList<Employee>();
    private final int MAX_STAFF = 10000;
    private int SALARY_TRESHOLD;

    public Employee find(String name){
        for (Employee e : arrEmployee){
            if(e.getName().equals(name)){
                return e;
            }
        }
        return null;
    }

    public String status(String name){
        if (find(name) != null){
            return find(name).getName() + " " + find(name).getSalary();
        }
        return "Karyawan tidak ditemukan";
    }

    public String add(String name, String position, int salary){
        if (arrEmployee.size() <= MAX_STAFF) {
            if (find(name) == null) {
                switch (position) {
                    case "MANAGER":
                        Employee m = new Manager(name, salary);
                        arrEmployee.add(m);
                        break;
                    case "STAFF":
                        Employee s = new Staff(name, salary);
                        arrEmployee.add(s);
                        break;
                    case "INTERN":
                        Employee i = new Intern(name, salary);
                        arrEmployee.add(i);
                        break;
                    default:
                        return "Jabatan tidak valid.";
                }
                return name + " mulai bekerja sebagai " + position + " di PT. TAMPAN";
            }
            return "Karyawan dengan nama " + name + " telah terdaftar";
        }
        return "Karyawan sudah melebihi batas maksimal";
    }

    public String addSubordinate(String meName, String subName){
        Employee senior = find(meName);
        Employee junior = find(subName);
        if (senior != null && junior != null){
            if (senior.getArrSubordinate().size() <= senior.getMAX_SUBORDINATE()){
                if (senior.canSupervise(junior)){
                    senior.addSubordinate(junior);
                    return "Karyawan " + subName + " telah menjadi bawahan " + meName;
                }
                return "Anda tidak layak memiliki bawahan";
            }
            return "Bawahan anda sudah melebihi batas";
        }
        return "Nama tidak berhasil ditemukan";
    }

    public String promote(Employee e){
        Employee m = new Manager(e.getName(), e.getSalary());
        arrEmployee.set(arrEmployee.indexOf(e), m);
        return "Selamat, " + e.getName() + " telah dipromosikan menjadi MANAGER";
    }
    public String payday(){
        if (arrEmployee.size() > 0) {
            for (Employee e : arrEmployee) {
                e.payday();
                if (e instanceof Staff && e.getSalary() > SALARY_TRESHOLD) {
                    System.out.println(promote(e));
                }
            }
            return "Semua karyawan telah diberikan gaji";
        }
        return "Tidak ada karyawan yang terdaftar";
    }

    public void setSALARY_TRESHOLD(int SALARY_TRESHOLD) {
        this.SALARY_TRESHOLD = SALARY_TRESHOLD;
    }
}
