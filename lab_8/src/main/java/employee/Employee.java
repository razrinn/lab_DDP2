package employee;

import java.util.ArrayList;

public abstract class Employee {
    private String name;
    private int salary;
    private ArrayList<Employee> arrSubordinate = new ArrayList<Employee>();
    protected int MAX_SUBORDINATE;
    private int salaryCount = 0;

    /////////////////////////////////////////////////////////
    public Employee(String name, int salary){
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public ArrayList<Employee> getArrSubordinate() {
        return arrSubordinate;
    }

    public int getMAX_SUBORDINATE() {
        return MAX_SUBORDINATE;
    }

    /////////////////////////////////////////////////////////
    public void addSubordinate(Employee Subordinate){
        arrSubordinate.add(Subordinate);
    }

    public void payday(){
        this.salaryCount += 1;
        if (this.salaryCount % 6 == 0){
            raiseSalary();
        }
    }

    public void raiseSalary(){
        int newSalary = salary + salary / 10;
        System.out.println(name + " mengalami kenaikan gaji sebesar 10% dari " + salary + " menjadi " + newSalary);
        this.setSalary(newSalary);
    }

    public abstract boolean canSupervise(Employee e);

}
