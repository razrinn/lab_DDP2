package employee;

public class Intern extends Employee{
    private final int MAX_SUBORDINATE = 0;

    public Intern(String name, int salary){
        super(name, salary);
    }

    @Override
    public boolean canSupervise(Employee e) {
        return false;
    }
}
