package employee;

public class Manager extends Employee{
    private final int MAX_SUBORDINATE = 10;

    public Manager(String name, int salary){
        super(name, salary);
    }

    @Override
    public boolean canSupervise(Employee e) {
        if (e instanceof Staff || e instanceof Intern){
            return true;
        }
        return false;
    }
}
