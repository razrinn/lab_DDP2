package employee;

public class Staff extends Employee{
    private final int MAX_SUBORDINATE = 10;

    public Staff(String name, int salary){
        super(name, salary);
    }

    @Override
    public boolean canSupervise(Employee e) {
        if (e instanceof Intern){
            return true;
        }
        return false;
    }
}
