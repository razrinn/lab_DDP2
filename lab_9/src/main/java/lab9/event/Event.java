package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * A class representing an event and its properties
 */
public class Event implements Comparable<Event> {
    /**
     * Name of event
     */
    private String name;
    private GregorianCalendar beginTime;
    private GregorianCalendar endTime;
    private BigInteger costPerHour;

    public Event(String name, GregorianCalendar beginTime, GregorianCalendar endTime, BigInteger costPerHour) {
        this.name = name;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.costPerHour = costPerHour;
    }

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }


    /**
     * Accessor for the cost
     *
     * @return cost of the event
     */
    public BigInteger getCost() {
        return costPerHour;
    }

    /**
     * To return the string format of the object Event
     *
     * @return String of the event detailed with the time
     */
    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return this.name +
                "\nWaktu mulai: " + sdf.format(this.beginTime.getTime()) +
                "\nWaktu selesai: " + sdf.format(this.endTime.getTime()) +
                "\nBiaya kehadiran: " + costPerHour.toString();
    }

    /**
     * To check whether this or the other event is overlapping or not
     *
     * @param other is the event you want to add
     * @return true if they're not overlapping each other
     */
    public boolean isCompatible(Event other) {
        if (beginTime.before(other.beginTime) && endTime.before(other.beginTime)) {
            return true;
        } else if (other.beginTime.before(beginTime) && other.endTime.before(beginTime)) {
            return true;
        } else if (endTime.compareTo(other.beginTime) == 0 || other.endTime.compareTo(beginTime) == 0) {
            return true;
        }
        return false;
    }

    /**
     * Override compareTo method that exists in Comparable interface for sorting the arrayList
     *
     * @param event is the next event to compare
     * @return int compare result
     */
    @Override
    public int compareTo(Event event) {
        return beginTime.compareTo(event.beginTime);
    }

    /**
     * To clone the event on the arrayList of user's events
     *
     * @return the cloned event
     */
    public Event cloneEvent() {
        Event temp = new Event(this.name, this.beginTime, this.endTime, this.costPerHour);
        return temp;
    }
}
