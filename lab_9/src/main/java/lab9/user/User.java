package lab9.user;

import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class representing a user, willing to attend event(s)
 */
public class User {
    /**
     * Name of user
     */
    private String name;

    /**
     * List of events this user plans to attend
     */
    private ArrayList<Event> events;

    /**
     * Constructor
     * Initializes a user object with given name and empty event list
     *
     * @param name = user's name
     */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }

    /**
     * Accessor for name field
     *
     * @return name of this instance
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a new event to this user's planned events, if not overlapping
     * with currently planned events.
     *
     * @param newEvent = event to be added
     */

    public void addEvent(Event newEvent) {
        events.add(newEvent);
    }

    /**
     * Method getTotalCost to get the total cost the user needed to pay for the event
     *
     * @return BigInteger data type of the total cost
     */
    public BigInteger getTotalCost() {
        BigInteger totalCost = new BigInteger("0");
        for (Event e : events) {
            totalCost = totalCost.add(e.getCost());
        }
        return totalCost;
    }

    /**
     * Returns the list of events this user plans to attend,
     * Sorted by their starting time.
     * Note: The list returned from this method is a copy of the actual
     * events field, to avoid mutation from external sources
     *
     * @return list of events this user plans to attend
     */
    public ArrayList<Event> getEvents() {
        ArrayList<Event> clonedArr = new ArrayList<>();
        for (Event e : events) {
            clonedArr.add(e.cloneEvent());
        }
        Collections.sort(clonedArr);
        return clonedArr;
    }
}
