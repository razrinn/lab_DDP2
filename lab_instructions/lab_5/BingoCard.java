/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		if(getNumberStates()[num] == null){
			return "Kartu tidak memiliki angka " + num;
		}
		else if(getNumberStates()[num].isChecked()){
			return num + " sebelumnya sudah tersilang";
		}
		else{
			getNumberStates()[num].setChecked(true);
			for (int i = 0; i < 5; i++) {
				//horizontal
				if (getNumbers()[i][0].isChecked() && getNumbers()[i][1].isChecked() &&
					getNumbers()[i][2].isChecked() && getNumbers()[i][3].isChecked() &&
					getNumbers()[i][4].isChecked()) {
					setBingo(true);
					break;
				}
				//vertikal
				if (getNumbers()[0][i].isChecked() && getNumbers()[1][i].isChecked() &&
					getNumbers()[2][i].isChecked() && getNumbers()[3][i].isChecked() &&
					getNumbers()[4][i].isChecked()) {
					setBingo(true);
					break;
				}
				//diagonal dari kiri atas
				if (getNumbers()[0][0].isChecked() && getNumbers()[1][1].isChecked() &&
					getNumbers()[2][2].isChecked() && getNumbers()[3][3].isChecked() &&
					getNumbers()[4][4].isChecked()) {
					setBingo(true);
					break;
				}
				//diagonal dari kanan atas
				if (getNumbers()[4][0].isChecked() && getNumbers()[3][1].isChecked() &&
					getNumbers()[2][2].isChecked() && getNumbers()[1][3].isChecked() &&
					getNumbers()[0][4].isChecked()) {
					setBingo(true);
					break; 
				}
			}	
			
			return num +" tersilang";
	}
}
	
	
	public String info(){
		String hasil = "";
		for(int m = 0; m < 5; m++){
			for(int n = 0; n < 5; n++){
				hasil+= "| ";
				if(getNumbers()[m][n].isChecked()){
					hasil+= "X ";
				}
				else{
					hasil+=getNumbers()[m][n].getValue();
				}
				hasil+=" ";
			}
			hasil+= (m <4 ) ? "|\n" : "|";
		}
		return hasil;	
	}
	
	
	public void restart(){
		for(int m = 0; m < 5; m++){
			for(int n = 0; n < 5; n++){
				getNumbers()[m][n].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}
	

}
